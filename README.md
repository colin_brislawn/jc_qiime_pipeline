# Juniata College QIIME pipeline

This pipeline attempt to quickly move data through the first steps of the usearch and qiime pipelines.

* http://www.qiime.org
* http://www.drive5.com/usearch/

Created at Juniata College with funding from HHMI, it was originally developed by Colin Brislawn and overseen by Dr. Lamendella.

* http://jcsites.juniata.edu/faculty/lamendella/
* http://www.juniata.edu