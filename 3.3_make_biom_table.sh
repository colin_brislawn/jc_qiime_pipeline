#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N make_biom_table
#PBS -j oe

#===============================================#
# Make .biom table that is compatible with qiime
# June 13 2013
#===============================================#


#location of main data folder
workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load usearch
module load qiime-1.8.0
#run command(s):


#remove output files
rm -f otus/repset.nochimeras.OTUs.fasta
rm -f otus/otu.map.uc
rm -f otus/otu_table_mc2.txt
rm -f otus/otu_table_mc2.biom


# label OTUs
fasta_number.py otus/repset.nochimeras.fasta OTU_ > \
otus/repset.nochimeras.OTUs.fasta

# map the reads from split_libraries or add_qiime_lables onto the rep set of OTUs
usearch7 -usearch_global labeled/combined_seqs.fna -db otus/repset.nochimeras.OTUs.fasta \
-strand plus -id 0.97 -uc otus/otu.map.uc -threads 8

# make a table of OTUs
uc2otutab_mod.py otus/otu.map.uc > otus/otu_table_mc2.txt

# convert that table to biom format
biom convert --table-type="otu table" -i otus/otu_table_mc2.txt \
-o otus/otu_table_mc2.biom
