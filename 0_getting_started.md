#Getting Started

First off, make a folder to hold your raw sequence data inside of your data
folder. This folder should NOT be inside of the jc_qiime_pipeline folder. Once 
you make this folder, you can move into it.

	:::shell
	mkdir raw_data
	cd raw_data


##Download sequences from the internet

If your sequences are posted to a website, you can download them using wget.

Use a web browser to go to the web page with the list of all the files you
want to download. Then copy that URL and paste it into the command below. This 
command will download all files ending with .gz from that URL.

	:::shell
	wget -A gz -m -p -E -k -K -np URL-to-your-data
	
As these datasets are large, downloading may take some time.

##Extract sequences
After you all your seguence files in your raw_data folder, take a look at 
their extensions to figure out how to extract from their archives.  

Many files ending with .fastq.gz

	:::shell
	gzip -d *.gz
	

A single files ending with .tar.gz or .tz

	:::shell
	tar -zxvf name-of-the-file.tar.gz

Once your sequences have been extracted so they end in .fastq, 
you are ready to continue.

##Loading modules
The initial steps of this pipeline use a combination of usearch and qiime. We
need to tell the cluster to use this software by loading the correct modules.

	:::shell
	module load usearch/7.0.1090
	module load qiime

Once your sequences have been extracted so they end in .fastq and you have 
loaded the modules you need, you are ready to continue.
