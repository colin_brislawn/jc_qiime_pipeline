#Initial Analyses

* Load Qiime if not already loaded: module load qiime-1.8.0
* Work in the directory where your .biom file is found, or move the .biom
file to your current directory

##Getting stats on an OTU table

Before analyzing an OTU table, it is critical to determine a cut-off point for
the number of sequences/sample for downstream analysis. Qiime 1.8 employs the
script "biom summarize-table" to report the stats of an OTU table.

	:::shell
	biom summarize-table -i otu_table_mc2_w_tax.biom -o otu_table_stats.txt


##Produce a single rarified table at an even sampling depth

Rarefaction is a standardization technique to subsample all of your samples at
the same depth. Samples below your rarefaction depth will be discarded while
samples above will be resampled down to that number. As such, choosing a
rarefaction depth is a balancing act between keeping samples and keeping
sequences in those samples.

	:::shell
	single_rarefaction.py -i otu_table_mc2_w_tax.biom -o otu_even_35000.biom -d 35000

##Summarize Taxa

The OTU table is divided into taxonomic level and either relative abundance or
absolute abundance of each taxon in each sample is returned. The default
output of this script are taxa summaries in both tab delimited (.txt) and biom
formats. Taxa are summarized by L2 = phylum, L3 = class, L4 = order,
L5 = family, and L6 = genus.

	:::shell
	summarize_taxa.py -i otu_even_35000.biom -o summarize_tax_35000
	summarize_taxa.py -i otu_table_mc2_w_tax.biom -o summarize_tax_all


