#!/usr/bin/env python
# file: remove_unused_barcodes.py
# Written by Mike Robeson 06/11/2013 

# This script is intended to remove unused barcodes from a 'barcode.fastq'
# file that are not present in the corresponding 'assembled.fastq' file 
# post assembly. As not all paired-ed reads will assemble.
# Accepts data in the form of:
#
# @M00949:58:000000000-A47AR:1:1101:15407:1767 1:N:0:0
# TACGTAGGTGGCGAGCGTTGTCCGGAATTACTGGGTGTAAAGGGCGCGTAGG
# +
# D2A/F10/D1/A///AEEE/FE@//??FHDDGDBBFEFBB=FFFDFFFFFFC


from cogent.parse.fastq import MinimalFastqParser

def read_bc_to_dict(barcodes):
    """Returns {fastq.header:fastq_data}.
    
        barcodes : barcodes file handle
    """
    bc_dict = {}
    for data in MinimalFastqParser(barcodes, strict=False):
        curr_label = data[0].strip()
        curr_seq = data[1].strip()
        curr_qual = data[2].strip()
        bc_dict[curr_label] = (curr_seq, curr_qual)
    return bc_dict

def remove_unused_barcodes(assembly, bc_dict, outfile):
    """Writes barcodes to file that only exist in assembly.
       We process this way so that the barcode and assembly fastq 
       files are in the same order!
    """
    for data in MinimalFastqParser(assembly, strict=False):
        curr_label = data[0].strip()
        try:
            curr_seq,curr_qual = bc_dict[curr_label]
            fastq_string = '@%s\n%s\n+\n%s\n' % (curr_label, curr_seq,\
                                                 curr_qual)
            outfile.write(fastq_string)
        except KeyError:
            print "%s not found" % curr_label


if __name__ == '__main__':
    from sys import argv
    
    USAGE = """
    This script will remove barcodes that are not present in the final
    output of a paired-end assembler. This is to prepare the data for
    use in QIIME (split_libraries_fastq.py).
    
    To run this script type:
        python remove_unused_barcodes.py <barcodes.fastq> <paired.end.assembly.fastq> <updated.barcodes.outfile.fastq>
    """
    
    arguments = len(argv)
    
    if arguments < 4:
        print USAGE
    else:
        barcodes = open(argv[1],'U')
        assembly = open(argv[2],'U')
        updated_barcodes = open(argv[3],'w')
        
        barcode_data = read_bc_to_dict(barcodes)
        remove_unused_barcodes(assembly, barcode_data, updated_barcodes)
        barcodes.close()
        assembly.close()
        updated_barcodes.close()