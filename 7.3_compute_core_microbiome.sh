#!/bin/bash

#PBS -k o
#PBS -l nodes=1:ppn=2
#PBS -N collate_and_plot
#PBS -j oe

#===============================================#
# Compute Core Microbome
# 
# May 27 2014 	
#===============================================#
#	NODES AND PPN MUST MATCH ABOVE          #

NODES=1
PPN=2
JOBSIZE=10000

workdir=/home/399group2/workshop
cd $workdir

module load qiime-1.8.0
#run command(s):


#Compute the core microbiome for samples of the type biofilm, set minimum 
#fraction to 1%. Additional cores up to 100% will be produced. 
compute_core_microbiome.py -i otu_even_8375.biom --mapping_fp mini_meta.txt \
--valid_states "Sample_type:biofilm" -o  core_microbiome/biofilm --min_fraction_for_core 0.01

#Compute the core microbiome for samples of the type water, set minimum 
#fraction to 1%. Additional cores up to 100% will be produced. 
compute_core_microbiome.py -i otu_even_8375.biom --mapping_fp mini_meta.txt \
--valid_states "Sample_type:water" -o  core_microbiome/water --min_fraction_for_core 0.01