#!/bin/bash

#PBS -l nodes=1:ppn=32
#PBS -N alpha_diversity
#PBS -j oe

#===============================================#
# Run alpha diversity metrics on a folder of biom tables

# July 23 2013
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load qiime-1.8.0
#run command(s):

#clean output folder
rm -rf alpha_div


parallel_alpha_diversity.py -i alpha_rars -o alpha_div -O 32 \
-m chao1,observed_species,PD_whole_tree,heip_e -t otus/rep_set.tre

#The rep_set.tre file is produced as a by-product during the OTU picking step, 
#and is needed for the PD whole tree metric. Make sure you include the path to
#the rep_set.tre file from the working directory you set above. 
