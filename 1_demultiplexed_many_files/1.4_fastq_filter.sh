#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N fastq_filter
#PBS -j oe

#===============================================#
# Filters fastq files and outputs fasta files.
# July 22 2014 					
#===============================================#

#location of main data folder
workdir=/home/brislawn/lampe/data/
cd $workdir

module load usearch
#run command(s):

#remove old folder and make new one
rm -rf filtered
mkdir filtered

#Make a list of files ending with .fastq to filter.
#This file will be stored in the paired directory
cd paired
ls *.fastq > list.txt
cd ..

while read line;
do usearch7 -fastq_filter paired/$line -fastq_trunclen 253 \
-fastq_maxee .5 -fastaout filtered/$line.fasta -threads 8;
#these settings were chosen for the 16S V4 region, after pairing with -fastq_mergepairs
done < ./paired/list.txt
