#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N fastq_mergepairs
#PBS -j oe

#===============================================#
# Join ends of fastq files using userach.
# July 22 2014
#===============================================#

#location of main data folder
workdir=/home/brislawn/lampe/data
cd $workdir

module load usearch

#remove old folder and make new one
rm raw_data/list.txt
rm -rf paired
mkdir paired

#Make a list of files ending with .fastq to pair.
#This will overwrite the file called list.txt.
#NOTE: You may have to manually edit this list by hand to make sure it has
#the right sequences in the right order. If you DO, comment out this section.
cd raw_data
ls *.fastq > list.txt
cd ..

#loop through this this list, merging every pair of files
while read R1
do read R2
echo $R1
usearch7 -fastq_mergepairs raw_data/$R1 -reverse raw_data/$R2 \
-fastq_minovlen 200 -fastq_truncqual 3 -fastqout paired/$R1
#these settings were chosen for the 16S V4 region
#NOTE: because this does not include -fastq_maxdiffs, this will NOT remove 
#all bad sequences. You MUST follow this up with -fastq_filter.
done < raw_data/list.txt
