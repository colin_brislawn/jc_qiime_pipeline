#!/bin/bash

#PBS -l nodes=1:ppn=2
#PBS -N fastq_stats
#PBS -j oe

#===============================================#
# Combine all .fastq files in a folder
# and get stats on this combined file.
# July 22 2014
#===============================================#

workdir=/home/brislawn/lampe/data
cd $workdir
#then move into the raw_data folder
cd raw_data

module load usearch
#run comm

#remove combined file from previous run
rm -f combined_unfiltered_read2.fastq
rm -f combined_unfiltered_read1.fastq

#combine all .fastq files
cat *R1_001.fastq > combined_unfiltered_read1.fastq
cat *R2_001.fastq > combined_unfiltered_read2.fastq

#get stats on this combined file using usearch7
usearch7 -fastq_stats combined_unfiltered_read1.fastq -log combined_unfiltered_read1.log &
usearch7 -fastq_stats combined_unfiltered_read2.fastq -log combined_unfiltered_read2.log

#remove the combined file
rm -f combined_unfiltered_read1.fastq
rm -f combined_unfiltered_read2.fastq
