#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N fastq_stats
#PBS -j oe

#===============================================#
# Combine all .fastq files in a folder
# and get stats on this combined file.
# July 22 2014
#===============================================#

#location of paired files from previous script
workdir=/home/brislawn/lampe/data/paired
cd $workdir

module load usearch

#remove combined file from previous run
rm -f combined_paired.fastq

#combine all .fastq files
cat *.fastq > combined_paired.fastq

#get stats on this combined file using usearch7
usearch7 -fastq_stats combined_paired.fastq -log combined_paired.log

#remove the combined file
rm -f combined_paired.fastq
