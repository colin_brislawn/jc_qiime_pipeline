#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N add_qiime_labels
#PBS -j oe

#===============================================#
# Label folder of fasta files from a
# (mini)metadata file that includes files names.
# Feb 4 2013 
#===============================================#

workdir=/home/brislawn/lampe/data
cd $workdir


module load qiime-1.8.0
#run command(s):

#make output folder
rm labeled -rf
mkdir labeled

#Before labeling, make sure your metadata or mini metadata file is in a 
#valid format by running the script validate_mapping_file.py.

#Add labels to identify which sample every sequences is from.
#Make sure the path to your metadata file is correct.
add_qiime_labels.py -m meta_072314.txt \
-i filtered -c InputFileName -n 1000000 -o labeled

#Verify that your sequences are lebeled correctly.
validate_demultiplexed_fasta.py -m meta_072314.txt \
-i labeled/combined_seqs.fna -o labeled