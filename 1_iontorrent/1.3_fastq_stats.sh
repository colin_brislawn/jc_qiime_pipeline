#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N fastq_stats
#PBS -j oe

#===============================================#
# Get stats on each .fastq file in a folder.
#
# July 27 2014
#===============================================#

#location of main data folder
workdir=/home/brislawn/iontorrent/data
cd $workdir
#then move into the raw_data folder
cd filtered

module load usearch
#run comm

#remove old log files
rm -f *.log

#Make a list of files ending with .fastq to analyse.
#This will overwrite the file called list.txt.
ls *.fastq > list.txt

#get stats on every file in this list using usearch7
while read R1
do echo $R1
usearch7 -fastq_stats $R1 -log $R1.log -fastq_qmax 45
#the flag -fastq_qmax 45 should be added for IonTorrent data
done < list.txt
