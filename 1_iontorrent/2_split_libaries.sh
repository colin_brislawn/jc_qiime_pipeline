#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N split_libraries
#PBS -j oe

#===============================================#
# Will demultiplex seqs in a .fastq using 
# barcodes and SampleIDs in a metadata file.
# Sept 11 2014
#===============================================#

#location of main data folder
workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load qiime-1.8.0
#run comm

#clean output folder
rm -rf labeled
mkdir labeled

#Before splitting, make sure your metadata or mini metadata file is in a 
#valid format by running the script validate_mapping_file.py.


split_libraries.py -f filtered/maxee_3_non_demultiplexed.fasta \
-o labeled -m metadata.txt -b 10

mv labeled/seqs.fna labeled/combined_seqs.fna

#Validation is usually not needed, but can be useful.
#validate_demultiplexed_fasta.py -m metadata.txt \
#-i labeled/combined_seqs.fna -o labeled


