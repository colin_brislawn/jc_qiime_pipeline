#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N filter
#PBS -j oe

#===============================================#
# Filters fastq files and outputs fasta files.
# Feb 4 2013 					
#===============================================#

#location of main data folder
workdir=/home/brislawn/iontorrent/data
cd $workdir

module load usearch
#run command(s):

#remove old folder and make new one
rm -rf filtered
mkdir filtered

#experimental settings for iontorrent
usearch7 -fastq_filter raw_data/non_demultiplexed.fastq \
-fastq_maxee 3 -fastaout filtered/maxee_3_non_demultiplexed.fasta

#other possible settings
:'
usearch7 -fastq_filter raw_data/non_demultiplexed.fastq \
-fastq_maxee 4 -fastqout filtered/maxee_4_non_demultiplexed.fastq

usearch7 -fastq_filter raw_data/non_demultiplexed.fastq \
-fastq_maxee 5 -fastqout filtered/maxee_5_non_demultiplexed.fastq
'