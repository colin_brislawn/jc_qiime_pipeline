#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N build_tree
#PBS -j oe

#===============================================#
# Build a phylogenetic tree from aligned and filtered
# sequences using pynast and fasttree. 
#
# June 16 2013
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir
#we will then move into the otus/ folder
cd otus

module load qiime-1.8.0
module load usearch
#run command(s):


#clean outputs
rm -rf pynast_aligned_seqs
rm -f rep_set.tre

# Align sequences
parallel_align_seqs_pynast.py -i repset.nochimeras.OTUs.fasta -o pynast_aligned_seqs \
-T --jobs_to_start 8

# Filter alignment
filter_alignment.py -i pynast_aligned_seqs/repset.nochimeras.OTUs_aligned.fasta \
-o pynast_aligned_seqs

# Build phylogenetic tree 
make_phylogeny.py -i pynast_aligned_seqs/repset.nochimeras.OTUs_aligned_pfiltered.fasta \
-o rep_set.tre
