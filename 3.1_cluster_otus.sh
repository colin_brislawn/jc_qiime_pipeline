#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N cluster_otus
#PBS -j oe

#===============================================#
# Cluster OTUs using the UPARSE pipeline
# (de-novo)
# June 13 2013 	
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load usearch
#run command(s):

#clean outputs
rm -f labeled/seqs.derep.fna
rm -f labeled/seqs.derep.mc2.fasta
rm otus -rf
mkdir otus


# dereplicate seqs
usearch7 -derep_fulllength labeled/combined_seqs.fna -output labeled/seqs.derep.fna \
-sizeout -threads 8

# sort with a min cutoff of 2 (sequences appearing once are discarded)
usearch7 -sortbysize labeled/seqs.derep.fna \
-minsize 2 -output labeled/seqs.derep.mc2.fasta

# cluster OTUs
usearch7 -cluster_otus labeled/seqs.derep.mc2.fasta \
-otus otus/repset.fasta
