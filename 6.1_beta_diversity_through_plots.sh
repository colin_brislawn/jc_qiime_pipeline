#!/bin/bash

#PBS -l nodes=1:ppn=16
#PBS -N beta_div
#PBS -j oe

#===============================================#
# Visualize beta diversity through plots.
# May 27 2014
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load qiime-1.8.0
#run command(s):

#clean output folder
rm -rf beta_div


#The first script generates distance matrices, coordinates frames, and 3D plots:
beta_diversity_through_plots.py -i otus/otu_table_mc2_w_tax.biom \
-t otus/rep_set.tre -m metadata.txt -o beta_div/ -a -O 16

#The second script generates 2D plots from the coordinate frame produced above. 
#Note: we are only using weighted distances. 
make_2d_plots.py -i beta_div/weighted_unifrac_pc.txt -m metadata.txt -o beta_div/2D/
