#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N update_barcodes
#PBS -j oe

#===============================================#
# Updates the index of barcodes after pairing 
# and filtering.
# June 13 2014 					
#===============================================#

#location of main data folder
workdir=/home/brislawn/uparse/data
cd $workdir

module load qiime-1.8.0
#Some dependancies of qiime are needed by this python script

# This script will attempt to copy the python script
# remove_unused_barcodes.py into the working directory 
# set above. You can copy it manually, if needed.
cp $PBS_O_WORKDIR/remove_unused_barcodes.py $workdir

./remove_unused_barcodes.py \
raw_data/BaseCalls_I1_001.fastq \
filtered/BaseCalls.fastq \
filtered/updated_barcodes.fastq