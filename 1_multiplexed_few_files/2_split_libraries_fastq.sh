#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -N sl_fastq
#PBS -j oe

#===============================================#
# Will demultiplex seqs in .fastq(.gz) files using 
# barcodes in another file and SampleIDs in a metadata file.
# June 13 2014
#===============================================#

#location of main data folder
workdir=/home/brislawn/uparse/data
cd $workdir

module load qiime-1.8.0
#run comm

#clean output folder
rm -rf labeled
mkdir labeled

#Before splitting, make sure your metadata or mini metadata file is in a 
#valid format by running the script validate_mapping_file.py.

#If paired ends were joined/merged, use updated barcodes
#No quality filtering is used because -fastq_filter was already used

split_libraries_fastq.py -i filtered/BaseCalls.fastq \
-o labeled/combined_seqs.fna -m metadata.txt -b filtered/updated_barcodes.fastq \
--rev_comp_mapping_barcodes -p 0 -q 0

