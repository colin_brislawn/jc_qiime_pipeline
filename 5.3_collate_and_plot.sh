#!/bin/bash

#PBS -l nodes=1:ppn=2
#PBS -N collate_and_plot
#PBS -j oe

#===============================================#
# Collates a folder of alpha diversity metrics 
# and plots the results as rarefaction plots.
# July 23 2014 	
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load qiime-1.8.0
#run command(s):

#clean output folder
rm -rf alpha_collated/
rm -rf alpha_plots/


#First, remove extra folder inside alpha_div which confuse qiime
rm -rf alpha_div/alpha_RAR*
rm -rf alpha_div/ALDIV*

#Second, collate the computed alpha diversity files
collate_alpha.py -i alpha_div/ -o alpha_collated/

#Then, generate alpha rarefaction plots. These can be visualized in a web 
#browser. Script requires a metadata file. For convenience, copy the metadata 
#file into the working directory. 
make_rarefaction_plots.py -i alpha_collated -m metadata.txt -o alpha_plots
