#!/bin/bash

#PBS -l nodes=1:ppn=32
#PBS -N multiple_rars
#PBS -j oe

#===============================================#
# Produce multiple rarified tables 
# for calculation of alpha diversity
#
# July 23 2013
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir

module load qiime-1.8.0
#run command(s):

#clean output folder
rm -rf alpha_rars/

#filter out samples with less then 10,000 reads.
#filter_samples_from_otu_table.py -i otus/otu_table_mc2_w_tax.biom \
#-o otus/filtered_otu_table_mc2_w_tax.biom -n 10000

#make rarefactions
parallel_multiple_rarefactions.py -i otus/otu_table_mc2_w_tax.biom \
-O 32 -o alpha_rars/ -m 200 -x 6200 -s 500 -n 20
