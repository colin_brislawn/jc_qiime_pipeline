#!/bin/bash

#PBS -l nodes=1:ppn=32
#PBS -N assign_tax
#PBS -j oe

#===============================================#
# Assigns taxonomy to reads from a rep set of otus
# and adds this information to a .biom table. 
#
# July 23 2013
#===============================================#

workdir=/home/brislawn/iontorrent/data/
cd $workdir
#we will then move into the otus/ folder
cd otus

module load qiime-1.8.0
module load usearch
#run command(s):

#clean output folder and .biom file
rm -rf rdp_assign_tax
rm -f otu_table_mc2_w_tax.biom

parallel_assign_taxonomy_rdp.py -i repset.nochimeras.OTUs.fasta -o rdp_assign_tax \
-T --jobs_to_start 20 \
-r /share/apps/qiime_1.8.0/gg_otus-13_8-release/rep_set/97_otus.fasta \
-t /share/apps/qiime_1.8.0/gg_otus-13_8-release/taxonomy/97_otu_taxonomy.txt
#make sure you are using the correct databases for your sequences

biom add-metadata -i otu_table_mc2.biom -o otu_table_mc2_w_tax.biom \
--observation-metadata-fp rdp_assign_tax/repset.nochimeras.OTUs_tax_assignments.txt \
--sc-separated taxonomy --observation-header OTUID,taxonomy
