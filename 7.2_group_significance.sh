#!/bin/bash

#PBS -k o
#PBS -l nodes=1:ppn=2
#PBS -N otu_category_significance
#PBS -j oe

#===============================================#
# Find significance of specific metadata variables.
# May 1 2014 	
#===============================================#
#	NODES AND PPN MUST MATCH ABOVE          #

NODES=1
PPN=2
JOBSIZE=10000

workdir=/home/399group2/workshop
cd $workdir

module load qiime-1.8.0
#run command(s):

group_significance.py -i otu_even_8375.biom -m mini_meta.txt \
-s kruskal_wallis -c Source -o kruskal_wallis_source_wholetable

group_significance.py -i summarized_tax/otu_even_8375_L2.biom -m mini_meta.txt \
-s kruskal_wallis -c Source -o kruskal_wallis_source_L2

group_significance.py -i summarized_tax/otu_even_8375_L4.biom -m mini_meta.txt \
-s kruskal_wallis -c Source -o kruskal_wallis_source_L4

group_significance.py -i summarized_tax/otu_even_8375_L2.biom -m mini_meta.txt \
-s mann_whitney_u -c Sample_type -o mann_whitney_Sample_type_L2

group_significance.py -i summarized_tax/otu_even_8375_L4.biom -m mini_meta.txt \
-s mann_whitney_u -c Sample_type -o mann_whitney_Sample_type_L4

group_significance.py -i summarized_tax/otu_even_8375_L5.biom -m mini_meta.txt \
-s mann_whitney_u -c Sample_type -o mann_whitney_Sample_type_L5