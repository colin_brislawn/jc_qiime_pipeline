#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -N chimera_check
#PBS -j oe

#===============================================#
# Identify and remove chimeras from the rep set
# June 13 2013
#===============================================#


#location of main data folder
workdir=/home/brislawn/iontorrent/data/
cd $workdir
#then we move into the otus folder
cd otus


module load usearch
#run command(s):


#remove output file
rm repset.nochimeras.fasta


#note the location of the gold.fa database. You may have to set this manually.
usearch7 -uchime_ref repset.fasta \
-db $PBS_O_WORKDIR/gold.fa -strand plus -threads 8 \
-nonchimeras repset.nochimeras.fasta

