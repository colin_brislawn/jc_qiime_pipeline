#!/bin/bash

#PBS -l nodes=1:ppn=2
#PBS -N compare_alpha
#PBS -j oe

#===============================================#
# Compare Alpha Diversity 
# Determine statistical significance between
# alpha diversity comparisons							
#===============================================#

workdir=/home/brislawn/lampe/data/
cd $workdir

module load qiime-1.8.0

#clean output directory
rm -rf alpha_comparisons
mkdir alpha_comparisons

#Compare alpha diversity is a very flexible script. You can chose both the 
#metadata category to compare and the alpha diversity metric from the alpha
#diversity metrics that you chose in calculating alpha diversity. Below are 
#several examples, but the options are nearly endless. Once you get a handle
#on running the script, it'd be efficient to write all of the comparisons you 
#want to do as separate scripts in one .sh file, such that they run in sequence. 

#Be sure to give the output file a very specific name relevant to what you 
#compared and by which metric. 

#NOTE: This script ONLY works on CATEGORICAL metadata parameters, such as 
#sample location, treatment type, or patient cohort;
#or DISCRETE QUANTITATIVE data, such as sampling day.

#Example: Compare alpha diversity between tissue types 
#(designed by the metadata column Tissue) according to the chao 1 metric.
compare_alpha_diversity.py -i alpha_collated/chao1.txt -m meta_072314.txt \
-c Tissue -o alpha_comparisons/Tissue_chao1

#Example: Compare alpha diversity between tissue types 
#(designated by the metadata column Tissue) according to the PD whole tree metric.
compare_alpha_diversity.py -i alpha_collated/PD_whole_tree.txt -m meta_072314.txt \
-c Tissue -o alpha_comparisons/Tissue_PD

compare_alpha_diversity.py -i alpha_collated/heip_e.txt -m meta_072314.txt \
-c Tissue -o alpha_comparisons/Tissue_heip_e

compare_alpha_diversity.py -i alpha_collated/observed_species.txt -m meta_072314.txt \
-c Tissue -o alpha_comparisons/Tissue_obs


