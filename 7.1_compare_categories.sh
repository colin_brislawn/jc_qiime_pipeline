#!/bin/bash

#PBS -k o
#PBS -l nodes=1:ppn=2
#PBS -N compare_categories
#PBS -j oe

#===============================================#
# Determine which metadata categories
# explain differences in beta diversity (unifrac distances)
#May 27 2014 	
#===============================================#
#	NODES AND PPN MUST MATCH ABOVE          #

NODES=1
PPN=2
JOBSIZE=10000

workdir=/home/399group2/workshop
cd $workdir

module load qiime-1.8.0
#run command(s):

compare_categories.py -i beta_div/weighted_unifrac_dm.txt \
--method anosim -m mini_meta.txt -c Sample_type -o anosim_Sample_type

compare_categories.py -i beta_div/weighted_unifrac_dm.txt \
--method anosim -m mini_meta.txt -c Source -o anosim_Source